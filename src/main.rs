use cargo_metadata::{MetadataCommand, CargoOpt};

trait ToStringOr {
    fn to_string_or<T: Into<String>>(&self, or: T) -> String;
}

impl ToStringOr for Option<String> {
    fn to_string_or<T: Into<String>>(&self, or: T) -> String {
        self.clone().unwrap_or_else(|| or.into())
    }
}

fn main() {
    let metadata = MetadataCommand::new()
        .manifest_path("./Cargo.toml")
        .features(CargoOpt::AllFeatures)
        .exec()
        .unwrap();

    let package = metadata.root_package().unwrap();

    println!("package_name {}", &package.name);
    println!("package_version {}", &package.version);
    println!("package_authors {}", &package.authors.join(", "));
    println!("package_description {}", &package.description.to_string_or("none"));
    println!("package_license {}", &package.license.to_string_or("none"));
    //println!("package.license_file {}", &package.license_file.to_string_or(""));
    //println!("package_categories {}", &package.categories.join(", "));
    //println!("package_keywords {}", &package.keywords.join(", "));
    println!("package_repository {}", &package.repository.to_string_or("none"));
    println!("package_homepage {}", &package.homepage.to_string_or("none"));
    println!("package_edition {}", &package.edition);
    println!("package_dependencies {}", &package.dependencies.len().to_string());
    for dependency in package.dependencies.iter() {
        println!("package_dependency_{}_version {}", &dependency.name, dependency.req.to_string());
        println!("package_dependency_{}_optional {}", &dependency.name, dependency.optional.to_string());
        println!("package_dependency_{}_kind {:?}", &dependency.name, dependency.kind);
        println!("package_dependency_{}_source {}", &dependency.name, dependency.source.to_string_or("none"));
        println!("package_dependency_{}_uses_default_features {}", &dependency.name, dependency.uses_default_features.to_string());
    }
}
